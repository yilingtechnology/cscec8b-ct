package com.cscec8b.ct.consumer;

import com.cscec8b.ct.common.bean.Consumer;
import com.cscec8b.ct.consumer.bean.CalllogConsumer;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/27 19:09
 * @history: 1.2019/1/27 created by chuhaitao
 */
public class BootStrap {

    public static void main(String[] args) throws Exception {

        Consumer consumer = new CalllogConsumer();
        consumer.consumer();
        consumer.close();

        //1、需求将被叫也需要当做记录添加到hbase中
        //2、查询指定人月份区间的通话记录
        //QuorumPeerMain
    }
}
