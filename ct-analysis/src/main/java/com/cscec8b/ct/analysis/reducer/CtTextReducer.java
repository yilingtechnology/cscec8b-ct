package com.cscec8b.ct.analysis.reducer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description: 数据分析的Reducer
 * @author: chuhaitao
 * @since: 2019/2/15 19:22
 * @history: 1.2019/2/15 created by chuhaitao
 */

public class CtTextReducer extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

        int sumCall = 0;
        int sumDuration = 0;

        for (Text text : values) {
            sumCall = sumCall + 1;
            sumDuration = sumDuration + Integer.valueOf(text.toString());
        }

        context.write(key, new Text(sumCall + "_" + sumDuration));
    }
}
