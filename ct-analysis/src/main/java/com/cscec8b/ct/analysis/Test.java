package com.cscec8b.ct.analysis;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/2/15 18:39
 * @history: 1.2019/2/15 created by chuhaitao
 */
public class Test {
    /**
     *需求：
     * 1.用户每天、每月、每年的通话总数，通话总时间
     * 2、亲密度：通话次数,通话时间
     *
     *
     * mysql表格的设计：
     *
     *
     *   id   tel    date       sumCall    sumTime
     *    1    133    201901        10        300
     *    1    133    20190101      100       3000
     *    1    133    2019          1000      30000
     *
     *
     *
     *   1、读取hbase的数据 ->Put对象：  使用rowKey
     * rowKey : 0_13956643892_20190211_13935457872_30000_1
     *
     * put对象
     *
     *
     *
     * mapper
     *   13956643892_20190211       1   dutation  30000
     *
     *
     * reducer  ：根据key聚合
     *
     *
     *
     * */


    //获取HBase的数据放入到Mysql中
    //编写mapper reducer
    //数据的流转     hbase->mapper中
}
