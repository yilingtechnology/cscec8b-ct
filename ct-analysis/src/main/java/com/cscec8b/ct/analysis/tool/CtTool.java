package com.cscec8b.ct.analysis.tool;


import com.cscec8b.ct.analysis.io.MysqlTextOutFormat;
import com.cscec8b.ct.analysis.mapper.CtTextMapper;
import com.cscec8b.ct.analysis.reducer.CtTextReducer;
import com.cscec8b.ct.common.constant.Names;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;


/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/2/15 19:23
 * @history: 1.2019/2/15 created by chuhaitao
 */
public class CtTool implements Tool {


    /**
     * 驱动程序调用是，会调用这个接口
     *
     * @param strings
     * @return
     * @throws Exception
     */
    public int run(String[] strings) throws Exception {


        Job job = Job.getInstance();
        //设置驱动类
        job.setJarByClass(CtTool.class);

        //只扫描     caller的列族
        Scan scan = new Scan();
        scan.addFamily(Bytes.toBytes(Names.CF_CALLER.getValue()));
        //设置mapper
        //hbase中初始化job的 mapper
        TableMapReduceUtil.initTableMapperJob(
                "teble",
                scan,
                CtTextMapper.class,
                Text.class,
                Text.class,
                job
        );

        //设置reducer
        job.setReducerClass(CtTextReducer.class);
        //设置最终输出的key
        job.setOutputKeyClass(Text.class);
        //设置最终输出的value
        job.setOutputValueClass(Text.class);
        //设置输出的格式
        job.setOutputFormatClass(MysqlTextOutFormat.class);

        //提交job
        Boolean result = job.waitForCompletion(true);


       // JobStatus.State.SUCCEEDED.getValue();
        return result ? 0 : 1;
    }

    /**
     * @param configuration
     */
    public void setConf(Configuration configuration) {


    }

    /**
     * @return
     */
    public Configuration getConf() {
        return null;
    }
}
