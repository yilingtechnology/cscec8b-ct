package com.cscec8b.ct.analysis;

import com.cscec8b.ct.analysis.tool.CtTool;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/2/15 19:25
 * @history: 1.2019/2/15 created by chuhaitao
 */
public class CtDriver {

    public static void main(String[] args) throws Exception {
        Configuration configuration = null;
        int result = ToolRunner.run(configuration, new CtTool(), args);
        System.out.println("运行结果：" + result);

    }
}
