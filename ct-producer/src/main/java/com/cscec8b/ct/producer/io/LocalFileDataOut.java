package com.cscec8b.ct.producer.io;

import com.cscec8b.ct.common.bean.DataOut;

import java.io.*;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 20:55
 * @history: 1.2019/1/26 created by chuhaitao
 */
public class LocalFileDataOut implements DataOut {

    private PrintWriter writer = null;

    public LocalFileDataOut(String path) {
        setPath(path);
    }


    public void writer(Object o) {
        writer(o.toString());
    }

    public void writer(String s) {
        writer.println(s);
        writer.flush();
    }

    public void setPath(String s) {

        try {
            writer = new PrintWriter((new FileOutputStream(new File(s))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        if (writer != null) {
            writer.close();
        }
    }
}
