package com.cscec8b.ct.producer.bean;

import com.cscec8b.ct.common.bean.Data;
import com.cscec8b.ct.common.bean.Val;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 21:12
 * @history: 1.2019/1/26 created by chuhaitao
 */
public class Contact extends Data {

    private String tel;
    private String name;


    @Override
    public void setValue(Object val) {
        content = (String) val;
        String[] values = content.split("\t");
        setTel(values[0]);
        setName(values[1]);

    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return
                "tel='" + tel + '\'' +
                ", name='" + name ;

    }
}
