package com.cscec8b.ct.producer.io;

import com.cscec8b.ct.common.bean.Data;
import com.cscec8b.ct.common.bean.DataIn;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 20:54
 * @history: 1.2019/1/26 created by chuhaitao
 */
public class LocalFileDataIn implements DataIn {


    private BufferedReader reader = null;

    public LocalFileDataIn(String path) {
        setPath(path);
    }

    public void setPath(String s) {

        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(s), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {

    }

    public Object read() throws IOException {

        return null;
    }

    public <T extends Data> List<T> read(Class<T> aClass) throws IOException {
     List<T> contacts=new ArrayList<T>();
        String line = null;
        try {

            while ((line = reader.readLine()) != null) {
                T t = aClass.newInstance();
                t.setValue(line);
                contacts.add(t);

            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return contacts;
    }
}
