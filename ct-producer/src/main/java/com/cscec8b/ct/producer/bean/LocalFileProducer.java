package com.cscec8b.ct.producer.bean;


import com.cscec8b.ct.common.bean.Data;
import com.cscec8b.ct.common.bean.DataIn;
import com.cscec8b.ct.common.bean.DataOut;
import com.cscec8b.ct.common.bean.Producer;
import com.cscec8b.ct.common.util.DataUtil;
import com.cscec8b.ct.common.util.NumberUtil;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 20:47
 * @history: 1.2019/1/26 created by chuhaitao
 */
public class LocalFileProducer implements Producer {


    private DataIn in;
    private DataOut out;
    private boolean flg = true;

    public void setIn(DataIn dataIn) {
        this.in = dataIn;
    }

    public void setOut(DataOut dataOut) {
        this.out = dataOut;
    }

    /**
     * 生产数据
     */
    public void producer() {

        try {
            //生产数据
            List<Contact> contacts = in.read(Contact.class);
            System.out.println(contacts.toString());


            while (flg) {
                //主叫
                int index1 = new Random().nextInt(contacts.size());
                int index2 = 0;
                while (true) {
                    index2 = new Random().nextInt(contacts.size());
                    if (index1 != index2) {
                        break;
                    }
                }

                Contact call1 = contacts.get(index1);
                Contact call2 = contacts.get(index2);
                //被叫

                //通话时间
                String startDate = "20180101000000";
                String endDate = "20190101000000";

                long starTime = DataUtil.parse(startDate, "yyyyMMddHHmmss").getTime();
                long endTime = DataUtil.parse(endDate, "yyyyMMddHHmmss").getTime();

                long time = starTime + (long) ((endTime - starTime) * Math.random());
                //通话时长
                String timeStr = DataUtil.format(new Date(time), "yyyyMMddHHmmss");


                String duration = NumberUtil.format(new Random().nextInt(3000), 4);
                //
                CallLog log = new CallLog(call1.getTel(), call2.getTel(), timeStr, duration);
                System.out.println(log);
                out.writer(log);
                Thread.sleep(500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void close() throws IOException {
        if (in != null) {

            in.close();
        }
        if (out != null) {

            out.close();
        }
    }
}
