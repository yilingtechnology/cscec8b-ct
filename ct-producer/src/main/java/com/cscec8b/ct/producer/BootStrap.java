package com.cscec8b.ct.producer;

import com.cscec8b.ct.common.bean.Producer;
import com.cscec8b.ct.producer.bean.LocalFileProducer;
import com.cscec8b.ct.producer.io.LocalFileDataIn;
import com.cscec8b.ct.producer.io.LocalFileDataOut;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description: 启动对象
 * @author: chuhaitao
 * @since: 2019/1/26 20:45
 * @history: 1.2019/1/26 created by chuhaitao
 */
public class BootStrap {

    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
            System.out.println("no args ");
            System.exit(1);
        }
        Producer producer = new LocalFileProducer();

        producer.setIn(new LocalFileDataIn(args[0]));
        producer.setOut(new LocalFileDataOut(args[1]));

        producer.producer();

        producer.close();
    }
}
