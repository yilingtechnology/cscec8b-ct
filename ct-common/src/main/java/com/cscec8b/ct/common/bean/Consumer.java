package com.cscec8b.ct.common.bean;

import java.io.Closeable;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/27 19:09
 * @history: 1.2019/1/27 created by chuhaitao
 */
public interface Consumer extends Closeable {

    void consumer() throws Exception;
}
