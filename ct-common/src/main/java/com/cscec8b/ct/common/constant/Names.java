package com.cscec8b.ct.common.constant;

import com.cscec8b.ct.common.bean.Val;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 20:40
 * @history: 1.2019/1/26 created by chuhaitao
 */
public enum Names implements Val {
    NAMESPACE("ct"),
    TABLE("ct:calllog"),
    CF_CALLER("caller"),
    CF_CALLEE("callee"),

    TOPIC("ct");


    private String name;

    private Names(String name) {
        this.name = name;
    }


    public void setValue(Object val) {
        this.name = (String) val;
    }

    public String getValue() {
        return name;
    }
}
