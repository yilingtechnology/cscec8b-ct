package com.cscec8b.ct.common.bean;

import java.io.Closeable;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 20:35
 * @history: 1.2019/1/26 created by chuhaitao
 */
public interface DataOut extends Closeable {

    void setPath(String path);

    void writer(Object obj);
    void writer(String  s);
}
