package com.cscec8b.ct.common.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/2/15 20:54
 * @history: 1.2019/2/15 created by chuhaitao
 */
public class JDBCUtil {

    private static String DRIVER = "com.mysql.jdbc.Driver";
    private static String URL = "jdbc:mysql://localhost:3306/ct?useUnicode=true&characterEncoding=UTF-8";
    private static String USERNAME = "root";
    private static String PASSWORD = "123456";

    /**
     * 获取连接
     *
     * @return
     */
    public static   Connection get() {
        Connection connection = null;

        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
