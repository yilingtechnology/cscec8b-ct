package com.cscec8b.ct.common.util;

import java.text.DecimalFormat;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/27 13:29
 * @history: 1.2019/1/27 created by chuhaitao
 */
public class NumberUtil {


    public static String format(int time, int length) {
        String str = new DecimalFormat("0000").format(time);
        return str;
    }
}
