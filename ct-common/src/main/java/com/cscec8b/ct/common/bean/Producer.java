package com.cscec8b.ct.common.bean;

import java.io.Closeable;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 20:34
 * @history: 1.2019/1/26 created by chuhaitao
 */
public interface Producer extends Closeable {

    void setIn(DataIn dataIn);

    void setOut(DataOut dataOut);

    /**
     * 生成数据
     */
    void producer();
}
