package com.cscec8b.ct.common.bean;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 20:35
 * @history: 1.2019/1/26 created by chuhaitao
 */
public interface DataIn extends Closeable {

    void setPath(String path);

    Object read() throws IOException;

    <T extends Data> List<T> read(Class<T> clazz) throws IOException;
}
