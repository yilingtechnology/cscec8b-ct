package com.cscec8b.ct.common.bean;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/26 20:33
 * @history: 1.2019/1/26 created by chuhaitao
 */
public interface Val {

    void setValue(Object val);
    Object getValue();
}
