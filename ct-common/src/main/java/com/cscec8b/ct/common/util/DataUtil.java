package com.cscec8b.ct.common.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/27 13:17
 * @history: 1.2019/1/27 created by chuhaitao
 */
public class DataUtil {


    public static Date parse(String s, String format) {
        SimpleDateFormat sdft = new SimpleDateFormat(format);
        Date data = new Date();
        try {
            data = sdft.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return data;
    }


    public static String format(Date date, String format) {
        SimpleDateFormat sdft = new SimpleDateFormat(format);
        return sdft.format(date);

    }
}
